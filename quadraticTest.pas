program quadraticTest;
  uses sysutils, quadratic;
  var
    a,b,c,discriminant: real;
    solutionOne, solutionOther: real;
  begin
    a := StrToFloat(ParamStr(1));
    writeln('a:', FormatFloat('0.00', a));
    b := StrToFloat(ParamStr(2));
    writeln('b:', FormatFloat('0.00', b));
    c := StrToFloat(ParamStr(3));
    writeln('c:', FormatFloat('0.00', c));
    discriminant := discriminantFind(a,b,c);
    writeln('discriminant: ', FormatFloat('0.00', discriminant));
    if discriminantPositive(discriminant) > 0 then
      begin
        solutionOne := solutionOnePositive(a,b,discriminant);
        writeln('solution one:', FormatFloat('0.00', solutionOne));
        solutionOther := solutionOtherPositive(a,b,discriminant);
        writeln('solution other:', FormatFloat('0.00', solutionOther));
      end
    else
      begin
        solutionOne := solutionBNegative(a,b);
        writeln('solution one:', FormatFloat('0.00', solutionOne));
        solutionOther := solutionDiscriminantNegative(a,discriminant);
        writeln('solution other:', FormatFloat('0.00', solutionOther));
        writeln('solution one:', FormatFloat('0.00', solutionOne));
        writeln('solution other:', FormatFloat('0.00', -solutionOther));
      end;
  end.
